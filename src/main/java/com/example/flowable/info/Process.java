package com.example.flowable.info;

import org.flowable.engine.repository.ProcessDefinition;

/**
 * @author Foolish
 * @description
 * @date: 2018/9/25 15:18
 */
public class Process {
    String id;//流程ID
    String rev;
    String category;//类别
    String name;//名称
    String key;//key
    int version;//版本
    String deploymentId;//部署ID
    String resourceName;//资源文件
    String dgrmResourceName;//流程图片
    String description;//说明
    boolean hasStartFormKey;//开始状态
    boolean hasGraphicalNotation;//图形表示
    boolean suspensionState;//暂停状态
    String tenantId;//租户标识
    String engineVersion;//引擎版本
    String derivedFrom;//来源
    String derivedFromRoot;//来自根
    int derivedVersion;//派生版本

    public Process(ProcessDefinition pd) {
        this.id = pd.getId();
        //this.rev = pd.;
        this.category = pd.getCategory();
        this.name = pd.getName();
        this.key = pd.getKey();
        this.version = pd.getVersion();
        this.deploymentId = pd.getDeploymentId();
        this.resourceName = pd.getResourceName();
        //this.dgrmResourceName = pd.;
        this.description = pd.getDescription();
        this.hasStartFormKey = pd.hasStartFormKey();
        this.hasGraphicalNotation = pd.hasGraphicalNotation();
        this.suspensionState = pd.isSuspended();
        this.tenantId = pd.getTenantId();
        this.engineVersion = pd.getEngineVersion();
        this.derivedFrom = pd.getDerivedFrom();
        this.derivedFromRoot = pd.getDerivedFromRoot();
        this.derivedVersion = pd.getDerivedVersion();
    }

    public Process(String id, String rev, String category, String name, String key, int version, String deploymentId, String resourceName, String dgrmResourceName, String description, boolean hasStartFormKey, boolean hasGraphicalNotation, boolean suspensionState, String tenantId, String engineVersion, String derivedFrom, String derivedFromRoot, int derivedVersion) {
        this.id = id;
        this.rev = rev;
        this.category = category;
        this.name = name;
        this.key = key;
        this.version = version;
        this.deploymentId = deploymentId;
        this.resourceName = resourceName;
        this.dgrmResourceName = dgrmResourceName;
        this.description = description;
        this.hasStartFormKey = hasStartFormKey;
        this.hasGraphicalNotation = hasGraphicalNotation;
        this.suspensionState = suspensionState;
        this.tenantId = tenantId;
        this.engineVersion = engineVersion;
        this.derivedFrom = derivedFrom;
        this.derivedFromRoot = derivedFromRoot;
        this.derivedVersion = derivedVersion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getDgrmResourceName() {
        return dgrmResourceName;
    }

    public void setDgrmResourceName(String dgrmResourceName) {
        this.dgrmResourceName = dgrmResourceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getHasStartFormKey() {
        return hasStartFormKey;
    }

    public void setHasStartFormKey(boolean hasStartFormKey) {
        this.hasStartFormKey = hasStartFormKey;
    }

    public boolean getHasGraphicalNotation() {
        return hasGraphicalNotation;
    }

    public void setHasGraphicalNotation(boolean hasGraphicalNotation) {
        this.hasGraphicalNotation = hasGraphicalNotation;
    }

    public boolean getSuspensionState() {
        return suspensionState;
    }

    public void setSuspensionState(boolean suspensionState) {
        this.suspensionState = suspensionState;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getEngineVersion() {
        return engineVersion;
    }

    public void setEngineVersion(String engineVersion) {
        this.engineVersion = engineVersion;
    }

    public String getDerivedFrom() {
        return derivedFrom;
    }

    public void setDerivedFrom(String derivedFrom) {
        this.derivedFrom = derivedFrom;
    }

    public String getDerivedFromRoot() {
        return derivedFromRoot;
    }

    public void setDerivedFromRoot(String derivedFromRoot) {
        this.derivedFromRoot = derivedFromRoot;
    }

    public int getDerivedVersion() {
        return derivedVersion;
    }

    public void setDerivedVersion(int derivedVersion) {
        this.derivedVersion = derivedVersion;
    }

    @Override
    public String toString() {
        return "Process{" +
                "id='" + id + '\'' +
                ", rev='" + rev + '\'' +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", version=" + version +
                ", deploymentId='" + deploymentId + '\'' +
                ", resourceName='" + resourceName + '\'' +
                ", dgrmResourceName='" + dgrmResourceName + '\'' +
                ", description='" + description + '\'' +
                ", hasStartFormKey=" + hasStartFormKey +
                ", hasGraphicalNotation=" + hasGraphicalNotation +
                ", suspensionState=" + suspensionState +
                ", tenantId='" + tenantId + '\'' +
                ", engineVersion='" + engineVersion + '\'' +
                ", derivedFrom='" + derivedFrom + '\'' +
                ", derivedFromRoot='" + derivedFromRoot + '\'' +
                ", derivedVersion=" + derivedVersion +
                '}';
    }
}
